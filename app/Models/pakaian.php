<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model;

class pakaian extends Model
{
    use HasFactory;

    protected $table = "pakaian";
    protected $fillable = [
        'merk',
        'jenis',
        'harga',
        'stok'
    ];
}
