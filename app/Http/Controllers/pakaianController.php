<?php

namespace App\Http\Controllers;
use DataTables;
use App\Models\pakaian;
use Illuminate\Http\Request;

class pakaianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getData(Request $request) 
    {
        $length = $request->length;
        $start = $request->start;
        $search = $request->search['value'];

        $columns = $request->columns;
        $order = $request->order;

        $order_col = $order[0]['column'];
        $order_dir = $order[0]['dir'];

        $result = new pakaian;

        if ($search) {
            $search = str_replace(asset(''), '', $search);
            $result = $result->where(function ($query) use ($search) {
                $query->where('merk', 'like', '%' . $search . '%')
                    ->orwhere('jenis', 'like', '%' . $search . '%')
                    ->orwhere('harga', 'like', '%' . $search . '%')
                    ->orwhere('stok', 'like', '%' . $search . '%');
            });
        }
        // $result = $result->with('parent', 'children');
        $result = $result->orderBy('id', 'asc');
        $result = $result->orderBy($columns[$order_col]['data'], $order_dir);

        $count = $result->count();
        $result = $result->take($length);
        $result = $result->skip($start);
        
        return response()->json([
            'recordsTotal' => $result->count(),
            'recordsFiltered' => $count,
            'data' => $result->get(),
        ], 200);

        // $getData = pakaian::all();
        // return response()->json([
        //     'data' => $getData
        // ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        pakaian::updateOrCreate(['_id' => $request->id],
                [
                    'merk' => $request->merk, 
                    'jenis' => $request->jenis, 
                    'harga' => $request->harga,
                    'stok' => $request->stok
                ]); 

        return response()->json(['success'=> 'Data berhasil ditambahkan'], 200);
    }

    public function edit($id)
    {
        $id = pakaian::find($id);
        return response()->json($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pakaian  $pakaian
     * @return \Illuminate\Http\Response
     */
    public function show(pakaian $pakaian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pakaian  $pakaian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pakaian $pakaian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pakaian  $pakaian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        pakaian::find($id)->delete();
     
        return response()->json(['success'=>'Delete data berhasil'], 200);
    }
}
