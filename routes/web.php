<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('index', [App\Http\Controllers\pakaianController::class, 'index']);
Route::post('getdata', [App\Http\Controllers\pakaianController::class, 'getData'])->name('index.getdata');
Route::post('index/store', [App\Http\Controllers\pakaianController::class, 'store'])->name('index.store');
Route::get('index/{id}/edit', [App\Http\Controllers\pakaianController::class, 'edit']);
Route::delete('index/{id}/delete', [App\Http\Controllers\pakaianController::class, 'destroy']);