<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan aja</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body>
    <div class="m-2">
        <a class="btn btn-success" href="javascript:void(0)" id="tambah"><i class="fa fa-plus" aria-hidden="true"></i>
            Tambah</a>
    </div>
    <div class="table-responsive m-2">
        <table id="data-table" width="100%" class="table table-bordered table-striped">
            <thead>
                <th>No.</th>
                <th>Merk</th>
                <th>Jenis</th>
                <th>Harga</th>
                <th>Stok</th>
                <th>Action</th>
            </thead>
        </table>
    </div>
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="pendaftarForm" name="pendaftarForm" class="form-horizontal">
                        <input type="hidden" name="id" id="id">

                        <div class="form-group">
                            <label for="merk" class="col-md-4 control-label">Merk</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="merk" name="merk" placeholder="Input merk"
                                    value="" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                        <label for="jenis" class="col-md-4 control-label">Jenis</label>
                            <div class="col-sm-12">
                                <select class="form-select" name="jenis" id="jenis">
                                    <option selected>Pilih</option>
                                    <option value="shirt">shirt</option>
                                    <option value="t-shirt">t-shirt</option>
                                    <option value="Hoodie">Hoodie</option>
                                    <option value="lainnya">lainnya</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="harga" class="col-md-4 control-label">Harga</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control" id="harga" name="harga"
                                    placeholder="Masukan Harga" value="" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="stok" class="col-md-4 control-label">Stok</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control" id="stok" name="stok"
                                    placeholder="Masukan Stok" value="" maxlength="50" required="">
                            </div>
                        </div>

                        <div class="mt-4 float-end">
                            <button type="button" id="batal" class="btn btn-secondary">Batal</button>
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
</script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(function () {
        // DATA TABLES
        var table = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('index.getdata') }}', // GET DATA FROM JSON
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}'
                }
            },
            columns: [{
                    data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + 1 + meta.settings._iDisplayStart;
                    }
                },
                {
                    data: 'merk',
                    name: 'merk',
                },
                {
                    data: 'jenis',
                    name: 'jenis'
                },
                {
                    data: 'harga',
                    name: 'harga'
                },
                {
                    data: 'stok',
                    name: 'stok'
                },
                {
                    render: function (data, type, row) {
                        return '\
                        <a href="javascript:void(0)" data-id="' + row._id + '" class="edit btn btn-warning"><i class="fa fa-pencil"></i></a>\
                        <a href="javascript:void(0)" data-id="' + row._id + '" class="destroy btn btn-danger btn-destroy"><i class="fa fa-trash-o"></i></a>\
                        '
                    }
                }
            ],
        });
        // SHOW MODAL
        $('#tambah').click(function () {
            $('#ajaxModel').modal('show');
            $('#saveBtn').val("create-data");
            $('#id').val('');
            $('#pendaftarForm').trigger("reset");
            $('#modelHeading').html("Tambah Data");
        });

        // HIDE MODAL
        $('#batal').click(function () {
            $('#ajaxModel').modal('hide');
        });

        // STORE DATA
        $('#saveBtn').click(function (e) {
            e.preventDefault();
            $(this).html('Memproses..');

            $.ajax({
                url: '{{ route('index.store') }}', // POST DATA TO JSON
                data: $('#pendaftarForm').serialize(),
                type: "POST",
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (data) {
                    $('#pendaftarForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    $('#saveBtn').html('Simpan');
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Simpan');
                }
            });
        });

        // EDIT DATA
        $('body').on('click', '.edit', function() {
            var id = $(this).data('id');
            $.get("{{ url('index') }}" +'/' + id +'/edit', function (data) {
                $('#modelHeading').html("Edit Book");
                $('#saveBtn').val("edit-pendaftar");
                $('#ajaxModel').modal('show');
                $('#id').val(data._id);
                $('#merk').val(data.merk);
                $('#jenis').val(data.jenis);
                $('#harga').val(data.harga);
                $('#stok').val(data.stok);
            })
        });

        // DELETE DATA
        $('body').on('click', '.destroy', function () {
        var id = $(this).data("id");
        var result = confirm("Are You sure want to delete !");
        if (result) {
            $.ajax({
                type: "DELETE",
                url: "{{    ('index') }}" + '/' + id + '/delete',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function (data) {
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        } else {
            return false;
        }
    });

    });

</script>

</html>
